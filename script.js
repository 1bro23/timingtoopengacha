let input = document.querySelector("input#totalItem")
let spanRand = document.querySelector("span#randomNumber")
let rangeInp=document.querySelector("input#range")
let delayInput = document.querySelector("input#delayTime")
let interval
let range=10
rangeInp.value=range
let delayInputValue=1000
let delayTime=1000

let target=document.querySelector("#randNumberList")
function insertRandNumber(target,n,val){
    for(let i=0;i<n;i++){
        let h4=document.createElement("h4")
        h4.innerHTML=val
        target.appendChild(h4)
    }
}
insertRandNumber(target,100,100)



let maxProbs=-100,minProbs=100
function suffleNumber(){
    range=rangeInp.value
    let cont = document.querySelector("#randNumberList")
    let prob=0;
    for(let i=0;i<cont.children.length;i++){
        let rand=Math.random()*input.value
        let el=cont.children[i]
        el.innerHTML=Math.round(rand)
        if(rand<range){
            el.style.color="darkorchid"
            prob++
        }
        else if(rand>input.value-range){
            el.style.color="crimson"
            prob-=.5   
        }
        else el.style.color="steelblue"
    }
    
    let probs=document.querySelector("#probs")
    if(prob/maxProbs>.85)probs.style.color="darkorchid"
    else probs.style.color="#fff"
    probs.innerHTML=prob+"%"

    if(maxProbs<prob){
        maxProbs=prob
        document.querySelector("#maxProbs").innerHTML=maxProbs+"%"
    }
    if(minProbs>prob){
        minProbs=prob
        document.querySelector("#minProbs").innerHTML=minProbs+"%"
    }
}

let suffleNumberIntv = setInterval(suffleNumber,1000)
window.onload=e=>{
    for(let i=0;i<100;i++)suffleNumber()
}
input.oninput=e=>{
    console.log("start random at max :"+e.target.value);
    clearInterval(suffleNumberIntv)
    suffleNumberIntv=setInterval(suffleNumber, 1000);
}
input.value=100
input.dispatchEvent(new Event("input"))
delayInput.oninput=e=>{
    delayInputValue=e.target.value*1000
    clearInterval(suffleNumberIntv)
    suffleNumberIntv=setInterval(suffleNumber, delayInputValue);
}